/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sudou;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JOptionPane.*;


public class Ventana extends javax.swing.JFrame {
    DefaultTableModel mod=null;
    JTable tabla=null;
    public Ventana() {
        super("ITM.5SA.Equipo Luis Tuyu");
        initComponents();
        setSize(600, 650);
        numero = 0;
        mod = new DefaultTableModel();
        tabla = new JTable(mod);
        
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        Entrada_val = new javax.swing.JComboBox<>();
        Calcular = new javax.swing.JButton();
        borrar = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        cantidad = new javax.swing.JLabel();

        jButton1.setText("jButton1");

        jLabel2.setText("jLabel2");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        jLabel1.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel1.setText("CANTIDAD");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(10, 10, 69, 17);

        Entrada_val.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        Entrada_val.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "3", "5", "7", "9", "11", "13", "15" }));
        Entrada_val.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Entrada_valActionPerformed(evt);
            }
        });
        getContentPane().add(Entrada_val);
        Entrada_val.setBounds(10, 30, 89, 23);

        Calcular.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        Calcular.setText("CALCULAR");
        Calcular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CalcularActionPerformed(evt);
            }
        });
        getContentPane().add(Calcular);
        Calcular.setBounds(120, 20, 105, 41);

        borrar.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        borrar.setText("BORRAR");
        borrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                borrarActionPerformed(evt);
            }
        });
        getContentPane().add(borrar);
        borrar.setBounds(240, 20, 99, 39);

        jLabel3.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel3.setText("DATOS:");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(410, 4, 60, 20);

        jLabel4.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel4.setText("Suma de filas o columnas:");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(380, 30, 150, 15);

        cantidad.setText("0");
        getContentPane().add(cantidad);
        cantidad.setBounds(530, 20, 50, 30);

        pack();
    }// </editor-fold>//GEN-END:initComponents
boolean c_boton=true;

    private void CalcularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CalcularActionPerformed
        if(c_boton){
        Metodos obj=new Metodos();
        String aux_num;
        aux_num = (String) Entrada_val.getSelectedItem();
        numero = Integer.parseInt(aux_num);
        obj.proceso(numero);
        generar_tabla(numero,obj.matriz);
        obj.reiniciar(numero);
        c_boton=false;
        cantidad.setText(obj.sumatoria(numero));
        }else{
                JOptionPane.showMessageDialog(null,"ERROR, DEBE BORRAR LA TABLA Y LUEGO OPRIMIR EL BOTON DE CALCULAR");
        }
    }//GEN-LAST:event_CalcularActionPerformed

    private void borrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_borrarActionPerformed
       c_boton=true;
        mod.setRowCount(0);
       mod.setColumnCount(0);
    }//GEN-LAST:event_borrarActionPerformed

    private void Entrada_valActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Entrada_valActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_Entrada_valActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Ventana().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Calcular;
    private javax.swing.JComboBox<String> Entrada_val;
    private javax.swing.JButton borrar;
    private javax.swing.JLabel cantidad;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    // End of variables declaration//GEN-END:variables
    private int numero;

    public void generar_tabla(int num,int[][]matriz) {
       
        String aux_dato[]=new String[num];
        
        
        for (int a = 0; a < num; a++) {
            mod.addColumn(a);//agregamos la posicion de las columnas y filas
        }

        for (int a = 0; a < num; a++) {
            for (int b = 0; b < num; b++) {
            aux_dato[b]=Integer.toString(matriz[a][b]);
            }
            mod.addRow(aux_dato);//añadiendo filas a la tabla
        }
        
        tabla.setRowHeight(40);
         tabla.setBounds(10,100,50*num,40*num);
        add(tabla);
        setSize(900, 800);
        //checar la medida de la tabla
       
      
       
        
    }
}
